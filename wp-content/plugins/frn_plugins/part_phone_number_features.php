<?php defined( 'ABSPATH' ) OR die( 'No direct access.' );

/* NOTES:
	Functions in this file handle all phone number design/CSS situations while maintaining proper linking for mobile devices and analytics labeling.

	Although this file is seperate, most of the other files depend or reference something from this file. 
	This file only includes frontend changes for users. 
	Admin features are still in the admin php.
	Move to it's own file on 3/7/17

	v6.3 - latest changes:
		Removed nowrap when text is defined
		Removed wasted processing by:
			Moving up everything that doesn't require phone number processing, 
			Moving variable definitions into the stages that refers to them,
			Moving up things that require minimal processing
			Relying on "return" to stop processing when what's remaining isn't necessary (instead of if/else)

	v7.1 - 9/14/17 - Added "label" to shortcode.


TABLE OF CONTENTS:
	CLEAN_UP code
	PHONE_PREP code 
	FRN_SHORTCODE
	AUTOSCAN
	JAVASCRIPT_SCAN



*/




//////////////////////////////////
// FRN_PHONE_SETUP FEATURES
//////////////////////////////////

add_shortcode( 'frn_phone', 'frn_phone_funct' );
add_action('wp_footer', 'frn_phone_js_scan', 3); //3 to make sure the script is added below chartbeat, etc.


//////////
// CLEAN_UP PHONE NUMBER
//Used in phone number and copyright shortcode functions below. Used to format the number friendly for all mobile phones.
//It will always return a number. If no number provided, it will pull from the settings directly.
if(!function_exists('frn_clean_number')) {
function frn_clean_number($number="") {
	$frn_number=trim($number);
	//Removed the script clean up since Rehab and Treatment no longer use the DialogTech SCRIPT approach to switching out numbers
	//if(strpos($number,"script")!==false) { 
		//Cleans it up to use as mobile phone number--works with images only
		if(trim($number)!=="") {
			$frn_number = trim(strip_tags(stripslashes($number))); //cleans out all HTML and quotes 
			$frn_number = preg_replace('/\D/',"",$frn_number); //cleans all but numbers (used second so that attributes with numbers are also removed)
		}
		//checking again because only text might have been provided
		//changed 8/31/17
		if(trim($frn_number)=="") {
			$options = get_option('site_phone_number');
			$frn_number = trim(strip_tags(stripslashes($options['site_phone']))); //cleans out all HTML and quotes
		}
		
		if($frn_number!=="") {
			$frn_number = str_replace(array(" ","(",")","-"),"",$frn_number);
			if(!is_numeric($frn_number)) $frn_number=preg_replace('/\D/',"",$frn_number); //strips all but numbers in case some text is left
			
			//Format the phone to still look pretty although the JS portion will reformat again
			if(strlen($frn_number)==11) {$frn_phoneFormat = '/^1?(\d)([0-9]..)([0-9]..)([0-9]...)$/'; //starts with 1
			  $frn_number = preg_replace($frn_phoneFormat,'$1-$2-$3-$4',$frn_number);}
			else if(strlen($frn_number)==10) {$frn_phoneFormat = '/([0-9]..)([0-9]..)([0-9]...)$/'; //doesn't start with 1
			  $frn_number = preg_replace($frn_phoneFormat,'1-$1-$2-$3',$frn_number);}
			else if(strlen($frn_number)==13){$frn_phoneFormat = '/([0-9]..)([0-9]..)([0-9]..)([0-9]...)$/'; //international
			  $frn_number = preg_replace($frn_phoneFormat,'$1-$2-$3-$4',$frn_number);}
			else if(strlen($frn_number)==14 && substr($frn_number,1)=="("){$frn_phoneFormat = '/([0-9]...)([0-9]..)([0-9]..)([0-9]...)$/'; //international
			  $frn_number = preg_replace($frn_phoneFormat,'$1-$2-$3-$4',$frn_number);}
		}
	//}
	return $frn_number;
}
}


////////////
// PHONE_Preperation for Functions
//
if(!function_exists('frn_phone_prep')) {
function frn_phone_prep($settings) {
	//used only to build what actually prints to the page.
	
	
	//Code to be wrapped (often blank or HTML with a class for CSS, but sometimes it's an image)
	if(!isset($settings[0])) $settings[0]="";
	$wrapped = trim($settings[0]);
	
	
	//wrapping items, declar vars
	$start=""; $end="";
	
	//Main trigger for linking phone numbers (otherwise, just return the wrapped content)
	$activate_linking = FALSE;
	global $frn_mobile;
	if($frn_mobile) {
		$phone_options = get_option('site_phone_number');
		if(!isset($phone_options['phone_linking'])) {
			$phone_options['phone_linking']="";
		}
		if($phone_options['phone_linking']=="N" && ($phone_options['auto_scan']=="D" || $phone_options['auto_scan']=="")) {
			$activate_linking=FALSE; // Keeping for confirmation of thought process
		}
		else {
			$activate_linking = TRUE;
		}
	}
	
	//applies only to mobile devices
	if($activate_linking) {
		
		/////////
		//prepare phone number
		

		/*
		SCENARIOS:
			1. provided by the shortcode (i.e. typcially a different number than what's in the FRN Settings)
			2. provided as the element to wrap by the link or SPAN (rare)
			3. relies on the number provided by the FRN Settings (most common)

		In all three scenarios, the number still needs to be cleaned so that it's recognized by all moble devices when clicked

		*/

		
		//1. See if specific number provided by shortcode
		if(!isset($settings[1])) $settings[1]="";
		$frn_number = trim($settings[1]);
		
		//2. If not provided and wrapped is, make the number equal to wrapped. The clean function will find the number in the wrapped content
		if($frn_number=="" && $wrapped!=="") $frn_number = $wrapped;

		// the clean function will return a number no matter what (if one is in the FRN Settings), so it's ok for this to be the final stage of assigning the number
		$frn_number = frn_clean_number(trim(strip_tags(stripslashes($frn_number))));
		if($frn_number=="0") $frn_number=""; //in rare cases a 0 is returned

		//set GA label if number provided is not in the FRN Settings
		//By this point, the frn_number is only blank if FRN Settings is blank
		$ga_phone_label = "Calls";
		if(isset($settings[4])) $ga_phone_label = trim($settings[4]);
		elseif($frn_number!=="") {
			$number_in_settings=frn_clean_number();
			if($frn_number!==$number_in_settings) {  // && preg_replace('/\D/',"",$frn_number)!==preg_replace('/\D/',"",$number_in_settings)
				$ga_phone_label=$frn_number;
			}
		}


		////////
		// Additional settings

		//DEFAULTS:
		$style="";
		$class=""; $id="";
		$title="";
		$lhn_tab_disabled="";
		$intl_prefix="";
		$ga_phone_category = "Phone Numbers";
		$ga_phone_location = "Phone Clicks [General]";
		


		//OVERWRITES
		if(isset($settings[2])) $ga_phone_category = trim($settings[2]);
		if(isset($settings[3])) $ga_phone_location = trim($settings[3]);
		if(isset($settings[5])) $style = trim($settings[5]);
		if(isset($settings[6])) $class = trim($settings[6]);
		if(isset($settings[7])) $id = trim($settings[7]);
		if(isset($settings[8])) $title = trim($settings[8]);
		if(isset($settings[9])) $lhn_tab_disabled = trim($settings[9]); //javascript only feature -- used to keep the main phoneBasic variable from being set on the page so that no number shows in the slideout
		if(isset($settings[10])) $intl_prefix = trim($settings[10]); //javascript only feature -- since Zander manually added in the international code in front of IfByPhone script, we had to add it to the number manually since the international pattern then couldn't be recognized
		
		if($id!=="") $id=' id="'.$id.'"';
		if($class!=="") $class=' class="'.$class.'"';

		//PHP vs. JavaScript Process??
		if($phone_options['phone_linking']!=="JS") {
			//JS version handles all of the GA code and wrapping numbers (if it worked)
			
			if($style=="" && $wrapped!=="") $style = ' style="white-space:nowrap;"';
			elseif($wrapped=="" && $style=="") $style="";
			elseif($style!=="") $style = ' style="'.$style.'"';
			else $style = ' style="white-space:nowrap;"';

			if($title!=="") $title = ' title="'.$title.'"';
			$ga_phone_label=",'".$ga_phone_label."'";
			//do not include "return false in an onClick unless you don't want the href followed"
			$start = ' <a '.$id.$class.$title.' href="tel:'.$frn_number.'"'.$style.' onClick="frn_reporting(\'phone\',\''.$ga_phone_category.'\', \''.$ga_phone_location.'\''.$ga_phone_label.');" >'; //if(typeof ga===\'function\') ga(\'send\', \'event\', \''.$ga_phone_category.'\', \''.$ga_phone_location.'\''.$ga_phone_label.'\'
			$end = "</a>";
		}

		/* 
		//Discontinued by Dax on 12/30/16 since the new DialogTech approach doesn't require using scripts in every phone number location.
		//It instead uses a phone number scan on the page to replace our number with theirs.
		elseif(strpos($wrapped,"script")!==false || $phone_options['phone_linking']=="JS") {
			//JavaScript approach (applies if "script" is in phone number field)
			if($ga_phone_category=="") $ga_phone_category='ga_phone_category="Phone Numbers" ';
				else  $ga_phone_category='ga_phone_category="'.$ga_phone_category.'" ';
			if($ga_phone_location=="") $ga_phone_location='ga_phone_location="Phone Clicks [General]" ';
				else  $ga_phone_location='ga_phone_location="'.$ga_phone_location.'" ';
			if($ga_phone_label!=="") $ga_phone_label='ga_phone_label="'.$ga_phone_label.'" ';
			if($lhn_tab_disabled!=="") $lhn_tab_disabled='lhn_tab_disabled="'.$lhn_tab_disabled.'" ';
			if($intl_prefix!=="") $intl_prefix = 'intl_prefix="'.$intl_prefix.'" ';
			
			if($style!=="") $frn_number_style = 'frn_number_style="'.$style.'" ';
			else $frn_number_style = "";
			
			$start = '<span id="frn_phones '.$id.'" '.$class.$frn_number_style.$ga_phone_category.$ga_phone_location.$ga_phone_label.$lhn_tab_disabled.$intl_prefix.' >';
			$end = "</span>";
		}
		*/

	}
	else {
		if($wrapped!=="") {
			$start = '<span '.$id.$class.' style="white-space:nowrap;" >';
			$end = "</span>";
		}
		else {
			$start = '<span '.$id.$class.' >';
			$end = "</span>";
		}
	}
	
	return $start.$wrapped.$end;
}
}



////////////
// Phone FRN_Shortcode: If shortcode found, this is what splits out the variables into our SPAN tags and phone number.
//shortcode requires "return" not "echo"
if(!function_exists('frn_phone_funct')) {
function frn_phone_funct($atts){
	extract( shortcode_atts( array(
		
		'number' => '', //overrides default number
		'only' => '', //if yes, only shows the number
		'text' => '', //substitutes for number when linking
		
		//Defaults
		'id' => '', //applied to <A> tag for text/phone number versions, or <IMG> tags for images
		'class' => '', //applied to SPAN, not links
		'style' => '', //newer version since easier to remember -- used for inline styles
		'css_style' => '', //older version prior to 2016 -- used for inline styles
		'title' => '', //added to links only -- i.e. smartphones only

		//Mobile overrides
		'mobile' => '',
		'mobile_text' => '', //here in case someone mistakes this to be like the rest
		'mobile_id' => '',
		'mobile_class' => '',
		'mobile_category' => '', //overrides Analytics defaults
		'mobile_action' => '', //overrides Analytics defaults

		//Desktop overrides
		'desktop' => '',
		'desktop_text' => '', //here in case someone mistakes this to be like the rest
		'desktop_url' => '', //if desktop text defined, it can link the text with a URL to go to a contact us page, for example
		'desktop_id' => '',
		'desktop_class' => '',
		'desktop_category' => '', //overrides Analytics defaults
		'desktop_action' => '', //overrides Analytics defaults

		//Analytics defaults
		'category' => '', //updated 2016 since other versions hard to remember, "ga_phone_category" version assigned to this later in code
		'action' => '', //updated 2016 since other versions hard to remember, "ga_phone_location" version assigned to this later in code
		'label' => '', //added 9/14/17 due to Rehab-International.org facility numbers
		'ga_phone_category' => 'Phone Numbers',
		'ga_phone_location' => 'Phone Clicks [General]',
		'ga_phone_label' => 'Calls', //changed to phone number if different than site default

		//Image options for when phone numbers are used in them (very popular on niche sites)
		//There is no mobile or desktop only image options
		'url' => '', //initial attribute, but since all other image attributes required "image", added image_url to keep things consistent. Kept since versions from 2013 or 2014 may still use it.
		'image_url' => '',
		'image_id' => '', //ID will be used if this is blank
		'image_class' => '',
		'image_style' => '',
		'image_title' => '',
		'alt' =>'', //was initial use but since all other image attributes required "image", added image_alt to keep things consistent. Kept since versions from 2013 or 2014 may still use it.
		'image_alt' =>'',

		//Likely not used anymore (prior to 2015)
		'intl_prefix'=> '', //was used on PPC only when testing international numbers -- made regex too hard when detecting number in text blocks
		'lhn_tab_disabled'=> '', //used to disable the chat tab if preferred on a specific page where this shortcode used. A forgotton feature by 2017.
		'frn_number_style' => '' //old option some rare situations may still use -- now just assigned to inline $style if it isn't defined

	), $atts, 'frn_phone' ) );
	

	/*
		NOTES:	There are over 20 user/design scenarios this shortcode considers. 
				The following list attempts to define the options, but not the user scenario since there are too many to list here. 
				But comments throughout the code attempts to talk about the various scenario combinations as it tries to handle them.

		General overall:
			ID
			Class
			Inline Style
			Number (can specify a number if different than FRN Settings)
			Desktops never are provided a link using a telephone number. They just display the number or text (that may or not be linked to a webpage)
			SPAN is always used to wrap a number
			Only = only returns the number in the FRN Settings (cleaned for using in a link or returned exactly as entered in FRN Settings)
			Only when text of phone numbers are linked, added are Analytics Event tracking values (predefined defaults, with overrides by use and device; e.g. if switching to a webpage link and text for desktops, Analytics event action switches to a version describing that situation)

		Text/CSS:
			title tag to clarify CSS icons if preferred to show when users hover
			Link text to phone number (only for mobile devices)
			No Text/CSS Only
			Desktop Switch: Can use a link to a page instead of a phone number
			Nowrap style only if phone number displayed

		Images:
			ID
			CSS
			Inline style
			ALT tag
			Mobile users will get an image linked with the phone number
			Desktop user can get a link to a webpage or nothing at all
			(This doesn't apply for background images via CSS option. Use text option instead.)

		Removed:
			Only possible by device type (e.g. only mobile or desktop)
			This option removes all code by device type.

		Device Consideration:
			Mobile/Tablet
			Desktop/Laptops
			(Not OS specific)
			(Not browser specific)

	*/

	// PRIMARY COMPONENTS
	$number=trim($number);
	$options = get_option('site_phone_number');


	/*
	////////
	/// STAGE 1 VARS: PREPARE TEXT FOR LINKING
	////////

	IMPORTANT NOTES FOR TEXT ATTRIBUTES
	TEXT VARIABLE:
		the text included is linked with the phone number and used for all platforms
		empty = removes all text, but keeps the link (best for CSS or image backgrounds)
		remove = 
			initially disabled the shortcode for desktop only
			but later we transitioned to a desktop-specific variable to clarify and maintain consistency.
			So, when setting the text variable above, text=remove cannot apply to mobile devices. 
			Only the mobile var should be allowed to trigger a removal for mobile.
	DESKTOP VARIABLE:
		the text included is only used for desktop versions. Mobile platforms will use the text values--whether default or custom.
		if using remove, then it means it'll be removed for desktops only. Mobile will still get the full linked text --whatever text is in the var
		if using empty, then it means the link or span will still be there but the text version will be used for mobile
	*/
	global $frn_mobile;

	//processed for every device in case "desktop" is the only text variable provided. 
	//Cancles the ability to use an image for mobile and text for desktop.
	$desktop=trim($desktop);
	$desktop_text=trim($desktop_text); //old version, but easier for some to remember
	if($desktop_text!=="") $desktop=$desktop_text;
	if($desktop!=="") $text=$desktop;
	
	if($frn_mobile) {
		$mobile=trim($mobile);
		$mobile_text=trim($mobile_text); //similar to desktop, added for consistency
		if($mobile_text!=="") $mobile=$mobile_text;
		if($mobile!=="") $text=$mobile;
		if($text=="" && $desktop!=="") $text=$desktop;
	}
	$text=trim($text);
	//cleaned versions to avoid lowering text for every test below
	$test_text = strtolower($text);
	$test_text_m = strtolower($mobile);


	/////////
	//PREPARE ID AND CLASS
		//if image scenario, then these could be used in that section of code
		//when preparing a linked number, these are passed to another function where the actual code is prepared
	if($frn_mobile) {
		$mobile_id=trim($mobile_id);
		$mobile_class=trim($mobile_class);
		if($mobile_id!=="") $id=$mobile_id;
		if($mobile_class!=="") $class=$mobile_class;
	}
	else {
		$desktop_id=trim($desktop_id);
		$desktop_class=trim($desktop_class);
		if($desktop_id!=="") $id=$desktop_id;
		if($desktop_class!=="") $class=$desktop_class;
	}
	$id=trim($id);
	$class=trim($class);


	/*
	//Phone_Linking Options
		PHP or blank = use PHP to link numbers
		JS = use JS to link numbers (doesn't work as of June 2017)
		N = don't link numbers, just display
		R = remove numbers
	*/
	if(!isset($options['phone_linking'])) $options['phone_linking']="";
	if($options['phone_linking']=="R") {
		//since we just want to remove all numbers, return nothing
		//This effectively disables the whole thing
		return "";
	}
	elseif($options['phone_linking']=="N") {
		//since we want to return a number, but never link it
		$wrapped_content=""; $span_style="";
		if($id!=="") $id=' id="'.$id.'"';
		if($class!=="") $class=' class="'.$class.'" ';
		if($text!=="" && $test_text!=="remove") {
			if($test_text_m!=="" && $frn_mobile) $test_text=$test_text_m;
			if($test_text=="empty") $text="";
			$wrapped_content=$text;
		}
		elseif($number!=="") {
			$wrapped_content = $number;
		}
		else {
			$wrapped_content = $options['site_phone'];
		}
		if($text=="") $span_style=' style="white-space:nowrap;"';
		//similar code is at the end, but keeping here for clarity
		//prior to 3/26/17, the span with the number didn't have id or class. Not sure if including them will cause issues on older sites
		return '<span'.$id.$class.$span_style.'>'.$wrapped_content.'</span>';
	}






	///////////////
	//
	// since the phone_linking setting is either blank, PHP, or JS, check the other elements that could also disable the feature...
	// CONTINUE PROCESSING...
	//
	////////////////
	


	
	
	


	//STAGE 2 VARIABLE PREP
	if (  (!$frn_mobile && $test_text=="remove")  ||  ($frn_mobile && $test_text_m=="remove")  ) {
		//don't do anything at this stage
		//this approached used to easily match with stages below
		//keeps the stage 2 vars from being process unncessarily
	}
	else {
		$url=trim($url);
		if($url!=="") $image_url=$url;
		$image_url=trim($image_url);

		if(!$frn_mobile) {
			$desktop_url=trim($desktop_url); //used to link the text to a page on the site
		}

		/////////
		/// ANALYTICS FEATURES and UNIFICATION
		//As more developers were involved, realized simplifying labels to match with GA was easier. March 2017
		//kept old variables in code below -- fewer code changes
		  $category=trim($category);
		  $action=trim($action);
		  $label=trim($label);
		  if($category!=="") $ga_phone_category=$category;
		  if($action!=="") $ga_phone_location=$action;
		  if($label!=="") $ga_phone_label=$label;

		  /// Device-Specific Overrides
		  if($frn_mobile) {
		  	$mobile_category=trim($mobile_category);
		  	$mobile_action=trim($mobile_action);
			if($mobile_category!=="") $ga_phone_category=$mobile_category;
			if($mobile_action!=="") $ga_phone_location=$mobile_action;
		  }
		  else {
		  	$desktop_category=trim($desktop_category);
		  	$desktop_action=trim($desktop_action);
			if($desktop_category!=="") $ga_phone_category=$desktop_category;
			if($desktop_action!=="") $ga_phone_location=$desktop_action;
		  }
		  //duplicate trim in case these ga_'s are used in shortcode directly
		  $ga_phone_location=trim($ga_phone_location);
		  $ga_phone_category=trim($ga_phone_category);
		  $ga_phone_label=trim($ga_phone_label);
	}




	/////////
	//// PHONE NUMBER PROCESSING NOT NEEDED FOR THESE STAGES
	/////////


	//If desktop and we have it set to remove everything, just return nothing. Added January 2017.
	//Added mobile attributes 6/30/17--after text=remove for desktops. 
	//But due to some sites already expecting Text=remove to affect desktops only, we have to maintain that situation. text=remove should not affect smartphones.
	$only=strtolower(trim($only));
	if (  (!$frn_mobile && $test_text=="remove" )  ||  ($frn_mobile && $test_text_m=="remove") || (!$frn_mobile && $test_text=="empty" && $id=="" && $class=="")  ) :
		//TESTING: return "<h1>Stage 1: Desktop only. Text or Desktop set to 'remove'. (mobile: ".$frn_mobile."; desktop: ".$desktop."; text:".$text."; desktop_url: ".$desktop_url.")</h1>";
		return "";
	
	//if it's a desktop and a contact url is provided, then we know no number processing is necessary. Added March 2017.
	//but if it's an image, the phone number may be in the alt tag, so it's skipped here by looking for the $url attribute
	elseif(!$frn_mobile && $desktop_url!=="" && $image_url=="") : 
		//TESTING: return "<h1>Stage 2: Desktop only. Text version. URL switcheroo. (mobile: ".$frn_mobile."; desktop: ".$desktop."; text:".$text."; desktop_url: ".$desktop_url.")</h1>";

		//Prepare TEXT attribute
		if($text=="") $text="Contact"; //if text not even defined, we need something in case it's forgotten
		elseif($test_text=="empty") $text="";

		if($id!=="") $id=' id="'.$id.'" ';
		if($class!=="") $class=' class="'.$class.'" ';

		//When this shortcode is used on a page like a normal link, it's no longer a conversion element
		//so it should be reported as a normal click to another page and NOT as a Google click event.
		//However, this can be overridden if we use the word "desktop" or "global" somewhere in the category label. (Global assumes it's used in our global contact options)
		if($desktop_action!=="") $ga_phone_location = $desktop_action;
		else $ga_phone_location = "Desktop Switch with: ".$desktop_url;
		$ga_phone_label = "Contact";

		$onclick=""; //sets it to avoid error log reporting
		if(strpos(strtolower($ga_phone_category),"global")!==false || strpos(strtolower($ga_phone_category),"desktop")!==false) $onclick=' onClick="frn_reporting(\'\',\''.$ga_phone_category.'\',\''.$ga_phone_location.'\',\''.$ga_phone_label.'\',false,\'\');"';
		
		//Since this is a link, it'll work like a normal pageview, but I wanted to record the event so we can evaluate contact options performance in one place
		return '<a href="'.$desktop_url.'" '.$id.$class.$onclick.' >'.$text.'</a>'; 
		//onClick="if(typeof ga===\'function\') ga(\'send\', \'event\', \''.$ga_phone_category.'\', \'Desktop Phone Number Switch with: '.$desktop_url.'\');" >'.$desktop.'</a>';

	elseif($only=="yes") :

		if($number!=="") $printed_number = $number;
		else $printed_number = trim($options['site_phone']);

		return stripslashes($printed_number);

	else : 



		///////
		//// PHONE NUMBER PROCESSING IS REQUIRED
		///////

			// Cases this is applied to:
			//	- no number provided in shortcode
			//  - it's a mobile device and "remove" is not used for $mobile var
			
			//The rest of this NOT for:
			//	if removing a link for desktop devices
			//	if text is linked to a page instead of a number

		//
		//TESTING: return "<h1>Stage 3: All devices // phone number processing is required // no 'remove' vars // desktop_url is empty (number: ".$number_from_db."; mobile: ".$frn_mobile."; desktop: ".$desktop."; text:".$text."; desktop_url: ".$desktop_url.")</h1>";


		////////
		//Prepare the three types of phone numbers
		if($only!=="cleaned") $number_from_db = frn_clean_number();
		if($number!=="") {
			$frn_number = frn_clean_number($number);
			if($only!=="cleaned") $printed_number = stripslashes($number);
		}
		else {
			//if not defined in the shortcode, then pull from FRN Settings
			$frn_number = $number_from_db;
			if($only!=="cleaned") $printed_number = trim(stripslashes($options['site_phone']));
		}




		////////
		// Prepare for display

		if($only=="cleaned") {
			//stage 2b; 
			//used the processed version of the site's number or the specified number
			//often used in links for phone numbers
			return $frn_number; 
		}
		else {
			
			//TESTING: return "<h1>Stage 3c: Mobile or Desktop // No onlys // no 'remove' vars // desktop_url is empty (mobile: ".$frn_mobile."; desktop: ".$desktop."; text:".$text.")</h1>";

			//if number set is different than what's in the settings, use that as the label if it's not set in the shortcode
			//Now controlled via the frn phone prep fuction
			//if(($frn_number!==$number_from_db) && $ga_phone_label=="") $ga_phone_label = $printed_number;
			
			/*
			//No longer used as of 2016, but kept to keep international concepts in mind
			//$lhn_tab_disabled=""; $intl_prefix="";
			//if($lhn_tab_disabled!=="") $lhn_tab_disabled = ' lhn_tab_disabled="'.$lhn_tab_disabled.'"';
			//if($intl_prefix!=="") $intl_prefix=' intl_prefix="'.$intl_prefix.'"';
		
			//Old approach using SPANs and javascript
			//if($ga_phone_category!=="") $ga_phone_category=' ga_phone_category="'.$ga_phone_category.'"';
			//if($ga_phone_location!=="") $ga_phone_location=' ga_phone_location="'.$ga_phone_location.'"';
			//if($ga_phone_label!=="") $ga_phone_label=' ga_phone_label="'.$ga_phone_label.'"';
			//if($frn_number!=="") $frn_number = ' frn_number="'.$frn_number.'"';
			*/
			


			/////////
			// SET UP REMAINING VARIABLES

			//STYLE FEATURES
			  $style=trim($style);
			  $css_style=trim($css_style); //old version likely used on niche sites prior 2016
			  if($css_style!=="") $style=$css_style; 
			  $title=trim($title);
			
			//IMAGE FEATURES
			  $image_id=trim($image_id);
			  $image_class=trim($image_class);
			  $image_style=trim($image_style);
			  $image_title=trim($image_title);
			  $alt=trim($alt);
			  $image_alt=trim($image_alt);

			//likely no longer used -- prior to 2015
			$intl_prefix=trim($intl_prefix);
			  $lhn_tab_disabled=trim($lhn_tab_disabled);
			  $frn_number_style=trim($frn_number_style);




			/////////////
			/// IMAGES
			if($image_url!=="") {
				
				//TESTING: return "<h1>Stage 3da: Mobile or Desktop // Image ONLY // No onlys // no 'remove' vars // desktop_url is empty (mobile: ".$frn_mobile."; desktop: ".$desktop."; text:".$text.")</h1>";

				//consolidate image vars into one var
				if($alt!=="") $image_alt=$alt;
				if($title!=="") $image_title=$title;

				//Setting CLASS and ID
				//we specifically set image_id/class to keep them out of phone number processing and leave the others blank
				//initially, we used image_id, but it became too complex for designers to remember, so sticking with just ID and class now (as of 2015)
				if($image_id=="" && $id!=="") {$image_id=$id; $id="";} //we can't use ID since it's passed to the link. For images, we don't want IDs in links
				if($image_id!=="") $image_id=' id="'.$image_id.'"';
				
				if($class!=="" && $image_class=="") {$image_class=$class; $class="";}
				if($image_class!=="") $image_class=' class="'.$image_class.'"';
				
				//inline style for number only
				if(strtolower($style)=="none") $style="";
					elseif($style!=="") $style=' style="'.$style.'"';
				
								/// LIKELY NOT USED ANYWHERE BY 2016
								//Check if domain bases are used (likely deprecated as of 2016, but didn't search 2015 redesigned sites for usage, so keeping activated)
								if(stripos($image_url,"%%frn_imagebase%%")>=0 or stripos($image_url,"%%idomain%%")>=0) {
									$options_sitebase = get_option('site_sitebase_code');
									$site_base = site_url();
									if(isset($options_sitebase['frn_imagebase'])) {
										if(trim($options_sitebase['frn_imagebase'])!=="") $site_base =  trim($options_sitebase['frn_imagebase']);
									}
									$image_url=str_replace('%%frn_imagebase%%',$site_base,$image_url);
									$image_url=str_replace('%%idomain%%',$site_base,$image_url);
								}
								else if(stripos($image_url,"%%frn_sitebase%%")>=0 or stripos($image_url,"%%ldomain%%")>=0) {
									$options_sitebase = get_option('site_sitebase_code');
									$site_base = site_url();
									if(isset($options_sitebase['frn_sitebase'])) {
										if(trim($options_sitebase['frn_sitebase'])!=="") $site_base =  trim($options_sitebase['frn_sitebase']);
									}
									$image_url=str_replace('%%frn_sitebase%%',$site_base,$image_url);
									$image_url=str_replace('%%ldomain%%',$site_base,$image_url);
								}

				if(isset($_SERVER["HTTPS"])) {if ($_SERVER["HTTPS"] == "on") $image_url = str_replace('http://','https://',$image_url);}
				

				//Check if phone number FRN %% shortcode used in alt tag and title
				if($image_alt!=="") {
					$image_alt=str_replace('%%frn_phone%%',$printed_number,$image_alt);
					$image_alt=' alt="'.$image_alt.'"';
				}
				if($image_title!=="") {
					$image_title=' title="'.$image_title.'"';
					$image_title=str_replace('%%frn_phone%%',$printed_number,$image_title);
					$title=str_replace('%%frn_phone%%',$printed_number,$title);
				}
				
				//build image HTML
				$image='<img'.$image_id.$image_class.' src="'.$image_url.'"'.$image_title.$image_alt.$image_style.' />';
				
				//Prep it all by sending to the phone prep function to build it
				if($frn_mobile) 
					return frn_phone_prep(array($image,$frn_number,$ga_phone_category,$ga_phone_location,$ga_phone_label,$style,$class,$id,$title,$lhn_tab_disabled,$intl_prefix));
				else {
					if($desktop_url!=="") return '<a href="'.$desktop_url.'">'.$image.'</a>';
					else return $image; 
				}

			}
			else {






				///////////
				// TEXT version (printing a phone number or text)
				///////////

				/*
					first figure out what should be linked
					then determine if it should be linked (desktop default is not to link a phone number)
					Then add ids and classes
				*/


				///////
				/// Prepare Text 
				

				//if($test_text=="empty") $text="";
				//elseif($text=="") $text="Contact"; //not sure why here. Was keeping the phone number from being printed on desktops.
				// $id -- don't define id here. It's defined in the frn_phone_prep or if no number, in the span section below.

				//inline styles
				if($style=="" && $css_style!=="") $style=$css_style;
				if($frn_number_style!=="" && $style=="") $style=$frn_number_style; //old approach no one could remember
				if(strtolower($style)=="none") $style="";
					elseif($style!=="") $style=' style="'.$style.'"';

				if($intl_prefix!=="") $intl_prefix=' intl_prefix="'.$intl_prefix.'"'; //used for JS processing of numbers ONLY
				
				if($text!=="") {
					//if it's a desktop and test_text=empty, it would have been taken care of early in the code since phone number processing is unnecessary
					//if text=remove, no matter the device, it's already handled earlier
					//so by this point if it's mobile and text=empty, that's the only case we need to handle here
					if($test_text_m=="empty" || $test_text=="empty") $printed_number="";
					else $printed_number=$text;
				}

				//return "<h1>Stage 3db: All devices (or smartphone && desktop=remove) // TEXT ONLY // no images (number: ".$printed_number."; mobile: ".$frn_mobile."; desktop: ".$desktop."; text:".$text."; image_url: ".$image_url.")</h1>";
				//If it's a smartphone, we'll link the phone number
				if($frn_mobile) return frn_phone_prep(array($printed_number,$frn_number,$ga_phone_category,$ga_phone_location,$ga_phone_label,$style,$class,$id,$title,$lhn_tab_disabled,$intl_prefix));

			} //end detection if image or not
		} //ends the more complicated processing section

		/*
		//two reasons this code would be used:
			1. Mobile, but phone number linking turned off
			1. desktop with text = empty (also means there is no phone number processing)
			2. the phone number is not linked
		*/
		if($class!=="") $class=' class="'.$class.'" ';
		if($id!=="") $id=' id="'.$id.'" ';
		//if printed text is a phone number, add the nowrap style
		$span_style="";
		if($style=="") {
			if($text=="" && $printed_number!=="") {
				if( (!$frn_mobile && $test_text!=="empty") || ($frn_mobile && $test_text_m!=="empty")) {
					$span_style=' style="white-space:nowrap;" ';
				}
			}
		}
		else {
			$span_style=$style;
		}
		//prior to 3/26/17, the span with the number didn't have id or class. Not sure if including them will cause issues on older sites
		return '<span'.$id.$class.$span_style.'>'.$printed_number.'</span>';

	endif; //ends requirement that text NOT be "remove" with desktop
} // ends function
}


/////////
// Phone AutoScan: If turned on, this will scan only content or the entire page for phone number patterns for only mobile devices and turn touches on them into trackable events and to always look like links.
/////////
if(!function_exists('frn_phone_autoscan_content')) {
function frn_phone_autoscan_content( $content ) {
	//PHP option: simply acts like a shortcode in that it looks for a number pattern and then replaces it with the spans and number again
	/*
	Will not work with letters (helps with pattern identification and mobile conversions)
	You can change out dashes for spaces or periods
	International:
	0000 0000
	00 00 00 00
	00 000 000
	00000000
	00 00 00 00 00
	+00 0 00 00 00 00
	00000 000000
	+00 0000 000000
	(00000) 000000
	+00 0000 000000
	+00 (0000) 000000
	00000-000000
	00000/000000
	000 0000
	000-000-000
	0 0000 00-00-00
	(0 0000) 00-00-00
	0 000 000-00-00
	0 (000) 000-00-00
	000 000 000
	000 00 00 00
	000 000 000
	000 000 00 00
	+00 00 000 00 00
	0000 000 000
	(000) 0000 0000
	(00000) 00000
	(0000) 000 0000
	0000 000 0000
	0000-000 0000
	0000 000 0000
	00000 000000
	0000 000000
	0000 000 00 00
	+00 000 000 00 00
	(000) 0000000
	+00 00 00000000
	000 000 000
	+00-00000-00000
	(0000) 0000 0000
	+00 000 0000 0000
	(0000) 0000 0000
	+00 (00) 000 0000
	+00 (0) 000 0000
	+00 (000) 000 0000
	(00000) 00-0000
	(000) 000-000-0000
	(000) [00]0-000-0000
	(00000) 0000-0000
	+ 000 0000 000000
	
	U.S. Versions:
	0 (000) 000-0000
	+0-000-000-0000
	0-000-000-0000
	000-000-0000
	(000) 000-0000
	000-0000
	0 (000) 000-0000 ext 1
	0 (000) 000-0000 x 1001
	0 (000) 000-0000 extension 2
	0 000 000-0000 code 3
	
	NOTE: To help with the autoscan, it requires a period, space, bracket, greater or less than, or parenthesis to be at the front and/or end of the number before it's found. 
		  Some links have numbers with periods in them that cause the autoscan feature to add the SPAN code within the links. This decreases the chances of that happening, but it also includes those characters in the linked text, but not the telephone link.
		  Primary resources: http://us3.php.net/preg_replace and http://us3.php.net/manual/en/function.preg-replace-callback.php
		  A phone pattern resources: http://stackoverflow.com/questions/17331386/php-preg-replace-phone-numbers ,  http://www.myspotinternetmarketing.com/javascript-php-and-regular-expressions-for-international-and-us-phone-number-formats/
	
	These are found, but are incorrect formats: Basically, make sure you don't have a dash, space, plus sign, or period before number or this will pull it in.
	   0-000-0000
	   -0-000-0000
	   -0-000-000-0000
	   -0 (000) 000-0000
	   +000 00-0-000-0000
	   3456789012345-6789
	   	8.8.8.8
		192.168.1.1
	*/
	global $frn_mobile;
	if($frn_mobile) {
		$options_phone = get_option('site_phone_number');
		$auto_scan="C";
		if(isset($options_phone['auto_scan'])) $auto_scan = $options_phone['auto_scan'];
		if($auto_scan!=="W" && $auto_scan!=="DA") { // && $auto_scan!=="A" Removed since the JavaScript entire page scanning isn't working.
			$pattern_in_content = "/(\s|\(|\[|\>)\d?(\s?|-?|\+?|\.?)((\(\d{1,4}\))|(\d{1,3})|\s?)(\s?|-?|\.?)((\(\d{1,3}\))|(\d{1,3})|\s?)(\s?|-?|\.?)((\(\d{1,3}\))|(\d{1,3})|\s?)(\s?|-?|\.?)\d{3}(-|\.|\s)\d{4}(\s|\)|\]|\.|\<)/s"; //makes sure there is a space or some other character on either side of the number
			//$content = preg_replace_callback($pattern, "preg_replace_clean_up", $content, 1);  //This helps make sure that characters outside of phone numbers aren't linked.
			//old (links the characters on either side of number as well: $content = preg_replace($pattern, $before."$0".$after, $content, 1);
			$content = preg_replace_callback($pattern_in_content,"phone_find_stage2", $content);
		}
	}
	return $content;
}
}
add_filter( 'the_content', 'frn_phone_autoscan_content');

if(!function_exists('phone_find_stage2')) {
function phone_find_stage2($found_pattern) {
	//Since this is looking within the pattern already found, we can be more flexible.
	$pattern_in_found = "!((\d )|(\d-))?(\(|\[)?(\b\+?[0-9()\[\]./ -]{7,17}\b|\b\+?[0-9()\[\]./ -]{7,17}\s+(extension|x|#|-|code|ext)\s+[0-9]{1,6})!i";
	return preg_replace_callback($pattern_in_found, function($matches){return frn_phone_prep(array($matches[0]));}, $found_pattern[0]);
}
}
if(!function_exists('frn_phone_autoscan_widget')) {
function frn_phone_autoscan_widget( $widget ) {
	//PHP option: simply acts like a shortcode in that it looks for a number pattern and then replaces it with the spans and number again
	global $frn_mobile;
	if($frn_mobile) {
		$options_phone = get_option('site_phone_number');
		$auto_scan="C";
		if(isset($options_phone['auto_scan'])) $auto_scan = $options_phone['auto_scan'];
		if($auto_scan=="WC" || $auto_scan=="W") {
			//Pull the matches
			$pattern = "/(\s|\(|\[|\>)\d?(\s?|-?|\+?|\.?)((\(\d{1,4}\))|(\d{1,3})|\s?)(\s?|-?|\.?)((\(\d{1,3}\))|(\d{1,3})|\s?)(\s?|-?|\.?)((\(\d{1,3}\))|(\d{1,3})|\s?)(\s?|-?|\.?)\d{3}(-|\.|\s)\d{4}(\s|\)|\]|\.|\<)/";
			$widget = preg_replace_callback($pattern, function($matches){return frn_phone_prep(array($matches[0]));}, $widget, 1);
		}
	}
	return $widget;
}
}
add_filter( 'widget_text', 'frn_phone_autoscan_widget');



/////////
// Phone JAVASCRIPT_SCAN Trigger
/////////
// Javascript phone detection feature only, not ever used so far
// Was initial version until realizing it slowed page load on the browser side of things
// Tested in June 2017 and it wasn't working. Helps when phone numbers are in PHP locations that can't be changed or in fields where shortcodes aren't possible.
if(!function_exists('frn_phone_js_scan')) {
function frn_phone_js_scan(){
// This script runs for every page load.
// Used to add phones2links.js to footer and see if entire page autoscan is turned on for JS file to scan the page for phone number formats and turn them into links using JS.
	global $frn_mobile;
	if($frn_mobile) {
		$frn_phone_ftr=""; $phone_linking="";

		//Turn on auto scanning
		$options_phone = get_option('site_phone_number');
		if(isset($options_phone['site_phone'])) $site_phone=$options_phone['site_phone'];
			else $site_phone="";

		//phone_linking = processing method
		if(isset($options_phone['phone_linking'])) $phone_linking=$options_phone['phone_linking'];
		$options_ua = get_option('site_head_code');
		$ga_ua = "_ua";
		if(isset($options_ua['frn_ga_ua'])) {
			if($options_ua['frn_ga_ua']!=="Activate") $ga_ua = ""; //Used to refer to other JS files that have the latest GA event and social tracking codes
		}
		
		//This feature was disabled due to the JavaScript replacing previously changed numbers in the sequence. So, A will never be an option.
		$auto_scan="C";
		if(isset($options_phone['auto_scan'])) $auto_scan = $options_phone['auto_scan'];
		if($auto_scan=="A") {
			//if to scan all of the page, set a global JS variable that will turn on the autoscan feature in our normal phones2links.js
			//See if content ID block is set
			if(isset($options_phone['js_content_id'])) $js_content_id=trim($options_phone['js_content_id']);
			if($js_content_id!=="") $js_content_id = ' js_content_id="'.trim($options_phone['js_content_id']).'";';
			
			$frn_phone_ftr .=  '
	<script type="text/javascript">frn_phone_autoscan="yes";'.$js_content_id.'</script>'; 
	
		}
		
		//Checks if JS turned on manually or phone number is added via JavaScript
		if($auto_scan=="A" || $phone_linking=="JS" ) { //removed || strpos($site_phone,"script")!==false from this IF statement since it's no longer use by DialogTech.
		$frn_phone_ftr .=  '
	<script async=\'async\' type="text/javascript" src="'.plugins_url().'/frn_plugins/part_phones2links'.$ga_ua.'.js"></script>
		';
		}
		
		echo $frn_phone_ftr;
	}
}
}







//////////////////////////////////
// DIALOG TECH SCRIPT/OPTIONS   //
//////////////////////////////////

//This script is added into the section where the wp_footer function is added in the PHP. 
//It's unlikely it's right before the /BODY tag. Dax isn't sure how that'll affect DT's find and replace of the phone numbers and Analytics reporting.
//The action that adds this to the footer is at the very bottom of this page with the rest of them
add_action('wp_footer', 'frn_dialogtech', 101);
if(!function_exists('frn_dialogtech')) {
function frn_dialogtech(){

	$options_phone = get_option('site_phone_number');
	if(!isset($options_phone['dialogtech'])) $options_phone['dialogtech']="";
	if(!isset($options_phone['dialogtech_id'])) $options_phone['dialogtech_id']="29b7c6a9512f3b1450d85acb9aa55fb8818ba6aa";

	if($options_phone['dialogtech']!=="") {
		$dialogtech ='
	<!-- #####
	DialogTech Service
	##### -->
	<script type="text/javascript">
        var _stk = "'.$options_phone['dialogtech_id'].'";
        (function(){
            var a=document, b=a.createElement("script"); b.type="text/javascript";
            b.async=!0; b.src=(\'https:\'==document.location.protocol ? \'https://\' :
            \'http://\') + \'d31y97ze264gaa.cloudfront.net/assets/st/js/st.js\';
            a=a.getElementsByTagName("script")[0]; a.parentNode.insertBefore(b,a);
        })();
    </script>
    <!-- #####
	END DialogTech Service
	##### -->



	';

		echo $dialogtech;

	}
}
}