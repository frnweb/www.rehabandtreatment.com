<?php defined( 'ABSPATH' ) OR die( 'No direct access.' );

/**
 * Plugin Name: FRN PPC Plugin
 * Description: This plugin must be used with the main FRN Plugin since it's setup as a submenu item. This plugin allows us to use the shortcode [frn_ppc_header] that parses the URL and creates a header for us so we don't have to have so many pages.
 * Version: 0.4
 * Updates: This version replaces center, clinic, facility, and program words with options. The prior update added states detection using an array of state names.
 * Author: Daxon Edwards / Foundations Recovery Network
 * Author URI: http://daxon.me
 */
 /*
	Copyright 2015  Foundations Recovery Network  ( email : frnservices@gmail.com )

	Tutorial source: http://ottopress.com/2009/wordpress-settings-api-tutorial/
	
	Each plugin needs the following to work:
	1. Admin
	2. Saving or storing data in fields
	3. On-Page
	
*/

//if($current_user->user_login!=="dax") error_reporting(E_ALL);

 
 
/////////////////////////////////////
// Creates admin area for settings  //
/////////////////////////////////////

//////
//This starts the whole process of adding things to the admin section of the site.
if ( is_admin() ){
	//initiates the build of the admin menu item and page
	add_action('admin_menu', 'plugin_ppc_admin_page');
}
function plugin_ppc_admin_page() {
	// Creates a submenu section under the main FRN Settings! menu and initiates the function that adds the variable to the database
	add_submenu_page( 'frn_features',"FRN Automatic Page Header Shortcode", "PPC Settings", 'manage_options', 'frn_ppc_page', 'frn_ppc_submenu_function');
	add_settings_section('frn_ppc_settings', "Manual Words to Search for in URL", 'frn_ppc_initial_content', 'frn_ppc_page');
		add_settings_field('frn_ppc_cities', "<small>Separate words or phrases by a comma. Commas aren't allowed in URLs. So if your phrase needs it, use the characters WordPress changes your comma to in the page's URL.</small>", 'frn_ppc_cities_field', 'frn_ppc_page', 'frn_ppc_settings');
		add_settings_field('frn_ppc_find_replace', "<small><b>FIND & REPLACE</b><br />Like the other, use a comma to separate the phrase you want to find with the phrase you want to replace. Also, use longer phrases to find first then list shorter phrases. Regarding the replacing phrase, it doesn't matter how long it is. To have the system delete a phrase found, just use a space after a comma.</small>", 'frn_ppc_fr_field', 'frn_ppc_page', 'frn_ppc_settings');
	add_action('admin_init', 'frn_ppc_setup');
}
function frn_ppc_submenu_function() {
	?>
	<h2>Foundations Recovery Network PPC Header Shortcode</h2>
	<p>Use the shortcode below anywhere you want the header of the page to show. It can be used in the title of a post, widgets, the content, Meta descriptions, and page excerpts. The default format for this is title case, but you have other options in case you want to use the same text in the page's content or any widget.</p>
	<h3>Limits:</h3>
	<ol>
	<li>Use "%27" in your web addresses where you want an apostrophe to show in the title. WordPress strips them out automatically. All other punctuation likely won't work. If you need others to work, let me know. It's easy to program.</li>
	<li>This will display whatever is between the first slash after the domain and the second slash. So, make sure if you use this in a widget that the widget is only on pages where you'd want it to work. If this becomes a challenge, I can add further restrictions into the code to look for certain page types.</li>
	</ol>
	<p>&nbsp;</p>
	<div class="wrap">
	<form action="options.php" method="post" class="frn_styles">
	<div class="frn_options_table">
	<h2>Shortcode:</h2>
	<p>Click on the shortcode option you want and copy it (works for most computers). Use the PHP option if you want to add it directly to a PHP template page when editing a theme.</p>
	<table class="frn_options_table">
		<tr>
			<td valign="top"></td>
			<td valign="top"><b><span id="frn_ppc_shortcode" class="frn_shortcode_sel" onClick="selectText('frn_ppc_shortcode')">[frn_ppc_header word="state"]</span></b></td>
		</tr>
		<tr>
			<td valign="top"></td>
			<td valign="top"><b><span id="frn_ppc_shortcode" class="frn_shortcode_sel" onClick="selectText('frn_ppc_shortcode')">&lt;?php echo do_shortcode('[frn_ppc_header]'); ?&gt;</span></b></td>
		</tr>
	</table>
	<p>&nbsp;</p>
	</div>
	
	<h3>Shortcode Options:</h3>
	<table>
		<tr><td width="120" valign="top"><li><span id="frn_ppc_lc" class="frn_shortcode_sel" onClick="selectText('frn_ppc_lc')"> type='lowercase'</span></li></td><td>To make the phrase all lowercase</td></tr>
		<tr><td width="120" valign="top"><li><span id="frn_ppc_uc" class="frn_shortcode_sel" onClick="selectText('frn_ppc_uc')"> type='uppercase'</span></li></td><td>To make everything uppercase</td></tr>
		<tr><td width="120" valign="top"><li><span id="frn_ppc_words" class="frn_shortcode_sel" onClick="selectText('frn_ppc_words')"> type='words'</span></li></td><td>To capitalize the first letter of very word</td></tr>
		<tr><td width="120" valign="top"><li><span id="frn_ppc_tfirst" class="frn_shortcode_sel" onClick="selectText('frn_ppc_tfirst')"> type='first'</span></li></td><td>To capitalize only the first letter of the phrase</td></tr>
		<tr><td width="120" valign="top"><li><span id="frn_ppc_wfirst" class="frn_shortcode_sel" onClick="selectText('frn_ppc_wfirst')"> word='first'</span></li></td><td>To show the first word. Capitalization is based on type.</td></tr>
		<tr><td width="120" valign="top"><li><span id="frn_ppc_last" class="frn_shortcode_sel" onClick="selectText('frn_ppc_last')"> word='last'</span></li></td><td>To show the last word. Capitalization based on type.</td></tr>
		<tr><td width="120" valign="top"><li><span id="frn_ppc_state" class="frn_shortcode_sel" onClick="selectText('frn_ppc_state')"> word='state'</span></li></td><td>
				<b>Default State List:</b>
					<ul style="list-style:inherit;">
					<li style="margin:0 0 0 20px;">This will display a state's name if one exists in the URL.</li>
					<li style="margin:0 0 0 20px;">States are searched alphabetically. The first one found is the one returned.</li>
					<li style="margin:0 0 0 20px;">States are automatically title case. The only other type option is uppercase.</li>
					</ul>
				<b>Manually Entered Items (IMPORTANT!):</b>
					<ul style="list-style:inherit;">
					<li style="margin:0 0 0 20px;">State names will not be searched if a manually listed item is found in the URL. So if a URL has both a state name and something in the manual list, the state won't be returned.</li>
					<li style="margin:0 0 0 20px;">Manually listed items are searched sequentially, starting from the first one in your list.</li>
					<li style="margin:0 0 0 20px;">If you search for the plural and singular forms of a word, put the plural version first. Otherwise, if you include a singular word first and the plural form is second and 
						the URL has the plural form in it, only the singular form will be returned since it's first in your list. </li>
					</ul>
			</td></tr>
	</table>
	
	<?php
		settings_fields( 'frn_ppc_settings' );
		do_settings_sections( 'frn_ppc_page' );
	?>
	<input name="Submit" type="submit" class="button button-primary" value="<?php esc_attr_e('Save Changes'); ?>" />
	</div>
	</form>
	<?php 
}
function plugin_options_ppc($input) {
	//saves values upon clicking update button in admin
	$newinput['list'] = trim($input['list']);
	$newinput['find_replace'] = trim($input['find_replace']);
	return $newinput;
}
function frn_ppc_initial_content() {
	//none for now. Just executing field function.
}
function frn_ppc_cities_field() {
	//Adds content to the frn_ppc_submenu page
	$frn_ppc_cities = get_option('frn_ppc_settings');
	?>
	<table class="frn_options_table">
		<tr>
			<td valign="top"><textarea id="frn_ppc_cities" name="frn_ppc_settings[list]" cols="65" rows="3" /><?=(isset($frn_ppc_cities['list']) ? $frn_ppc_cities['list'] : null); ?></textarea></td>
		</tr>
	</table>
	<?php
}
function frn_ppc_fr_field() {
	//Adds content to the frn_ppc_submenu page
	$frn_ppc_fr = get_option('frn_ppc_settings');
	?>
	<table class="frn_options_table">
		<tr>
			<td valign="top"><textarea id="frn_ppc_fr" name="frn_ppc_settings[find_replace]" cols="65" rows="3" /><?=(isset($frn_ppc_fr['find_replace']) ? $frn_ppc_fr['find_replace'] : null); ?></textarea></td>
		</tr>
	</table>
	<?php
}
function frn_ppc_setup() {
	//registers the variable for the database
	register_setting( 'frn_ppc_settings', 'frn_ppc_settings','plugin_options_ppc'  ); 
}










/////////////////////////////////
// On-page Component Rendering //
/////////////////////////////////

//////
// Phone Shortcode: If shortcode found, this is what splits out the variables into our SPAN tags and phone number.
//shortcode requires "return" not "echo"
function frn_ppc_header_funct($atts){
	extract( shortcode_atts( array(
		'type'=> '',
		'word'=> 'empty'
	), $atts, 'frn_ppc_header' ) );
		
		//$domain = str_replace("http://","",str_replace("https://","",get_bloginfo( 'wpurl' )));
		$debug=false;
		global $current_user;
		get_currentuserinfo();
		echo "
		
			<!--
			////////////////////
			////////////////////
			
			CURRENT DEBUG USER: ".$current_user->user_login."
			
			////////////////////
			////////////////////
			-->
		
			";
		//if($current_user->user_login=="daxon.edwards") $debug=true;
		
		
		$domain = get_bloginfo( 'wpurl' );
		if(!is_front_page() && (is_single() || is_page())) $pageURL = get_permalink(get_the_ID());
			else $pageURL = $_SERVER['REQUEST_URI'];
		$pageURL = str_replace($domain."/","",$pageURL);
		if(strpos($pageURL,"/")===0) $pageURL = substr($pageURL,(-1*(strlen($pageURL)))+1); //if starts with slash (for some reason), then just take everything after the slash as the pageURL. Not sure when this case occurs.
		//case: if no slash in url at all
		$first_slash = strpos($pageURL,"/");
		if($first_slash >=0) {
			
			$frn_ppc_settings = get_option('frn_ppc_settings');
			
			if(!strpos($pageURL,"/") && $pageURL!=="") $title = $pageURL;
			else $title = substr($pageURL,0,$first_slash); //pulls out left characters up to the point where a slash is found
			$title = str_replace("-"," ",$title);
			$title = str_replace("%27","'",$title);
			$title = strtolower(trim($title)); //makes sure things are flat
			$title_whole = strtolower(trim($title));
			
			
			//FIND & REPLACE PHRASES
				//This section was added 6/29/15 to help page titles be more accurate and so people can't say we are being dishonest
				//all of this was transitioned to the manual entry form field so it can be changed at anytime
				//array = item
				//(find,replace)
				//Original: 
			/*
			$find_replace_array_original = array(
				array("addiction center","addiction treatment options"),
				array("addiction clinic","addiction treatment options"),
				array("alcohol center","alcohol treatment options"),
				array("alcohol program","alcohol treatment options"),
				array("drug program","drug addiction treatment options"),
				array("inpatient clinic","inpatient treatment options"),
				array("inpatient facility","inpatient treatment options"),
				array("inpatient program","inpatient treatment options"),
				array("residential center","residential treatment options"),
				array("residential facility","residential treatment options"),
				array("residential program","residential treatment options"),
				array("program","options"),
				array("facility","options"),
				array("clinic","options"),
				array("center","options")
			);
			*/
			
			
			
			
			//FIND STATES and manual words to return
			$city_found=""; $state_found = "";
			if(strtolower($word)=="state") {
				$run_states = true; //default to process state names. Only is stopped if manual item found in URL.
				if(isset($frn_ppc_settings['list'])) {
					if(trim($frn_ppc_settings['list'])!=="") {
						//overrides state list
						//search using manual list
						$improve_commas = str_replace(", ",",",$frn_ppc_settings['list']); //makes sure there aren't spaces after the comma
						$manual_list = explode(",",$improve_commas); //creates array
						$total_cities = count($manual_list); $c=0;
						while ($city_found==="" && $c<>$total_cities) {
							if($manual_list[$c]!=="") {
							if(strpos($title,strtolower($manual_list[$c]))!== false) {
								$city_found = trim($manual_list[$c]);
								$title = $city_found;
								$run_states = false;
								if($debug) echo "
			
			<!--
			////////////////////
			////////////////////
					
				City Found: ".$manual_list[$c]."
				
			////////////////////
			////////////////////
				
			-->
			
								";
							}
							}
							$c++;
						}
					}
				}
				
				if($run_states) {
					$states = array (
						"Alabama",
						"Alaska",
						"Arizona",
						"Arkansas",
						"California",
						"Colorado",
						"Connecticut",
						"Delaware",
						"Florida",
						"Georgia",
						"Hawaii",
						"Idaho",
						"Illinois",
						"Indiana",
						"Iowa",
						"Kansas",
						"Kentucky",
						"Louisiana",
						"Maine",
						"Maryland",
						"Massachusetts",
						"Michigan",
						"Minnesota",
						"Mississippi",
						"Missouri",
						"Montana",
						"Nebraska",
						"Nevada",
						"New Hampshire",
						"New Jersey",
						"New Mexico",
						"New York",
						"North Carolina",
						"North Dakota",
						"Ohio",
						"Oklahoma",
						"Oregon",
						"Pennsylvania",
						"Puerto Rico",
						"Rhode Island",
						"South Carolina",
						"South Dakota",
						"Tennessee",
						"Texas",
						"Utah",
						"Vermont",
						"Virgin Islands",
						"West Virginia",
						"Virginia",
						"Washington DC",
						"Washington",
						"Wisconsin",
						"Wyoming"
					);
					//search using list in states array
					$total_states = count($states); $s=0;
					while ($state_found=="" && $s<>$total_states) {
						if(strpos($title,strtolower($states[$s]))!== false) {
							$state_found = trim($states[$s]);
							$title = $state_found;
							if($debug) echo "
			
			<!--
			////////////////////
			////////////////////
				
				State Found: ".$states[$s]."
							
			////////////////////
			////////////////////
			-->
			
							";
						}
						$s++;
					}
				}
				if($city_found=="" && $state_found=="") $title = "";
			}
			elseif(strtolower($word)=="first") $title = substr($title,0,strpos($title," "));
			elseif(strtolower($word)=="last") $title = substr($title,-1*(strlen($title)-strrpos($title," ")));
			
			//Pulls the list from the PPC Settings in the DB
			//if it's blank, then disable the rest
			if(trim($frn_ppc_settings['find_replace'])!=="") {
				$find_replace_array = explode("\n",trim($frn_ppc_settings['find_replace'])); //creates array of each line in the settings textbox
				$i=0; $word_found=false;
				while ($i<(count($find_replace_array))) {
					//removed $word_found==false && since we realized there were URL situations where we needed more than one replacement
					if($find_replace_array[$i]!=="") {
						$find_replace_items = explode(" => ",$find_replace_array[$i]); //creates individual arrays of the find & replace phrases
						$find=strtolower($find_replace_items[0]);
						if(strpos($title,$find)!==false) {
							$replace=$find_replace_items[1];
							$title = str_replace($find,$replace,$title);
							$word_found=true;
							if($debug) echo "
			
			<!--
			////////////////////
			////////////////////
			
				Find/Replace Found: ".$find_replace_items[0]."
			
			////////////////////
			////////////////////
			-->
			
			";
						}
					}
					$i++;
				} //loop
			}
			
			if(trim($title)!=="") {
				if($state_found!=="" && $type!="uppercase") $title = titleCase($title); //defaults to this if state is found
				elseif($type=="lowercase") $title = $title; //already lowercase...just making sure that the other things don't execute if these are true
				elseif($type=="uppercase") $title = strtoupper($title); //capitalizes every word
				elseif($type=="first") $title = ucfirst($title); //capitalizes only the first word
				elseif($type=="words") $title = ucwords($title); //capitalizes every word
				else $title = titleCase($title);  //the default -- title case
			}
			
			if($debug) {
				if(!strpos($pageURL,"/") && $pageURL!=="") $ext_title = $pageURL;
					else $ext_title = substr($pageURL,0,$first_slash);
					if($type=="") $type_notice="[not used]";
						else $type_notice=$type;
					if($word=="") $word_notice="[not used]";
						else $word_notice=$word;
				$debug_notice = "
			<!--
			////////////////////
			////////////////////
				
				
				VARIABLES: 
					Type: ".$type_notice."
					Word: ".$word_notice."
					Domain: ".get_bloginfo( 'wpurl' )."
					Domain No HTTP: ".str_replace("http://","",str_replace("https://","",get_bloginfo( 'wpurl' )))."
					Server Name: ".$_SERVER["SERVER_NAME"]."
					URI: ".$_SERVER["REQUEST_URI"]."
					Full URL: ".$_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"]."
					Permalink: ".get_permalink(get_the_ID())."
					Stripped URL: ".$pageURL."
					First Slash Pos: ".strpos($pageURL,"/")."
					Extracted Title: ".$ext_title."
					Cleaned Title: ".str_replace("-"," ",$ext_title)."
					Totals Chars: ".strlen($title_whole)."
					Last Word Start: ".strrpos($title_whole," ")."
					End Result: ".$title."
				
				
			////////////////////
			////////////////////
			-->
				";
			}
			
			//return $title." test ".$_SERVER['REQUEST_URI'];
		} //end check to make sure this doesn't start with a slash
		
		return $debug_notice.$title;
}

//original Title Case script © John Gruber <daringfireball.net>
//javascript port © David Gouch <individed.com>
//PHP port of the above by Kroc Camen <camendesign.com>

if(!function_exists('titleCase')) {
function titleCase ($title) {
	//remove HTML, storing it for later
	//       HTML elements to ignore    | tags  | entities
	$regx = '/<(code|var)[^>]*>.*?<\/\1>|<[^>]+>|&\S+;/';
	preg_match_all ($regx, $title, $html, PREG_OFFSET_CAPTURE);
	$title = preg_replace ($regx, '', $title);
	
	//find each word (including punctuation attached)
	preg_match_all ('/[\w\p{L}&`\'‘’"“\.@:\/\{\(\[<>_]+-? */u', $title, $m1, PREG_OFFSET_CAPTURE);
	foreach ($m1[0] as &$m2) {
		//shorthand these- "match" and "index"
		list ($m, $i) = $m2;
		
		//correct offsets for multi-byte characters (`PREG_OFFSET_CAPTURE` returns *byte*-offset)
		//we fix this by recounting the text before the offset using multi-byte aware `strlen`
		$i = mb_strlen (substr ($title, 0, $i), 'UTF-8');
		
		//find words that should always be lowercase…
		//(never on the first word, and never if preceded by a colon)
		$m = $i>0 && mb_substr ($title, max (0, $i-2), 1, 'UTF-8') !== ':' && 
			!preg_match ('/[\x{2014}\x{2013}] ?/u', mb_substr ($title, max (0, $i-2), 2, 'UTF-8')) && 
			 preg_match ('/^(a(nd?|s|t)?|b(ut|y)|en|for|i[fn]|o[fnr]|t(he|o)|vs?\.?|via)[ \-]/i', $m)
		?	//…and convert them to lowercase
			mb_strtolower ($m, 'UTF-8')
			
		//else:	brackets and other wrappers
		: (	preg_match ('/[\'"_{(\[‘“]/u', mb_substr ($title, max (0, $i-1), 3, 'UTF-8'))
		?	//convert first letter within wrapper to uppercase
			mb_substr ($m, 0, 1, 'UTF-8').
			mb_strtoupper (mb_substr ($m, 1, 1, 'UTF-8'), 'UTF-8').
			mb_substr ($m, 2, mb_strlen ($m, 'UTF-8')-2, 'UTF-8')
			
		//else:	do not uppercase these cases
		: (	preg_match ('/[\])}]/', mb_substr ($title, max (0, $i-1), 3, 'UTF-8')) ||
			preg_match ('/[A-Z]+|&|\w+[._]\w+/u', mb_substr ($m, 1, mb_strlen ($m, 'UTF-8')-1, 'UTF-8'))
		?	$m
			//if all else fails, then no more fringe-cases; uppercase the word
		:	mb_strtoupper (mb_substr ($m, 0, 1, 'UTF-8'), 'UTF-8').
			mb_substr ($m, 1, mb_strlen ($m, 'UTF-8'), 'UTF-8')
		));
		
		//resplice the title with the change (`substr_replace` is not multi-byte aware)
		$title = mb_substr ($title, 0, $i, 'UTF-8').$m.
			 mb_substr ($title, $i+mb_strlen ($m, 'UTF-8'), mb_strlen ($title, 'UTF-8'), 'UTF-8')
		;
	}
	
	//restore the HTML
	foreach ($html[0] as &$tag) $title = substr_replace ($title, $tag[0], $tag[1], 0);
	return $title;
}
}


if ( !is_admin() ){ //if page is not an admin page

	//shortcode works in main content areas by default. The following applies them to more areas.
	add_shortcode( 'frn_ppc_header', 'frn_ppc_header_funct' );
	
	///General filters to activate shortcodes -- applies to all shortcodes
	add_filter('widget_text', 'do_shortcode'); //in widget code
	add_filter('single_post_title ', 'do_shortcode'); //in post titles
	add_filter('the_excerpt ', 'do_shortcode'); //for excerpts in categories
	add_filter('wp_title ', 'do_shortcode'); //for page title
	add_filter('wp_head ', 'do_shortcode');  //for meta description

}
?>