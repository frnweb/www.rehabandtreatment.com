		<section class="testimonials desktop-tc mobile-no ">
			<h2 class="is-center">Our Staff</h2>

			<div class="gallery items-3">
  				<div id="item-3" class="control-operator"></div>
  				<div id="item-4" class="control-operator"></div>

  				<div class="secondary-controls">
					<div class="superfluous">
						<nav>
							<a class="arrow-previous staff" href="#item-3"></a>
							<a class="arrow-next staff" href="#item-4"></a>
						</nav>
					</div>
				</div>
		
			  <figure class="item our-staff-1">
			  		<div>
					<img src="/wp-content/themes/Rehab-And-Treatment/mobile-images/the-canyon-staff/amanda-diguilio-Primary-Therapist.png"><p class="name">Amanda Diguilio</p><p>Primary Therapist</p>
					</div>
					<div>
					<img src="/wp-content/themes/Rehab-And-Treatment/mobile-images/the-canyon-staff/Anna-Aslanian-Primary-Therapist.png"><p class="name">Anna Aslanian</p><p>Primary Therapist</p>
					</div>
					<div>
					<img src="/wp-content/themes/Rehab-And-Treatment/mobile-images/the-canyon-staff/Deborah-Rosenberg-Office-Manager.png"><p class="name">Deborah Rosenberg</p><p>Office Manager</p>
					</div>
					<div>
					<img src="/wp-content/themes/Rehab-And-Treatment/mobile-images/the-canyon-staff/Joel-Morris-Consulting-Psychiatrist.png"><p class="name">Joel Morris</p><p>Consulting Psychiatrist</p>
					</div>
			  </figure>

			  <figure class="item our-staff-1">
			  		<div>
					<img src="/wp-content/themes/Rehab-And-Treatment/mobile-images/the-canyon-staff/lynn-marie-saso-ClinicalDirector.png"><p class="name">Lynn Marie Saso</p><p>Clinical Director</p>
					</div>
					<div>
					<img src="/wp-content/themes/Rehab-And-Treatment/mobile-images/the-canyon-staff/Melinda-Drake-Director.png"><p class="name">Melinda Drake</p><p>Director</p>
					</div>
					<div>
					<img src="/wp-content/themes/Rehab-And-Treatment/mobile-images/the-canyon-staff/Rodney-Aquino-Patient-Care-Coordinator.png"><p class="name">Rodney Aquino</p><p>Patient Care Coordinator</p>
					</div>
			  </figure>

			  <div class="controls">
			    <a href="#item-3" class="control-button">•</a>
			    <a href="#item-4" class="control-button">•</a>
			  </div>
			</div>
			
		
	</section>

	<section class="testimonials chicago-staff mobile-only">
		
					<h2 class="is-center">Our Staff</h2>

			<div class="gallery items-3">
  				<div id="item-30" class="control-operator"></div>
  				<div id="item-31" class="control-operator"></div>
  				<div id="item-32" class="control-operator"></div>
  				<div id="item-33" class="control-operator"></div>
  				<div id="item-34" class="control-operator"></div>
  				<div id="item-35" class="control-operator"></div>
  				<div id="item-36" class="control-operator"></div>

  				<div class="secondary-controls">
					<div class="superfluous">
					</div>
				</div>
		
			  <figure class="item our-staff-1">
			  		<div>
			  		<a class="arrow-previous staff" href="#item-37"></a>
					<img src="/wp-content/themes/Rehab-And-Treatment/mobile-images/the-canyon-staff/amanda-diguilio-Primary-Therapist.png"><p class="name">Amanda Diguilio</p><p>Primary Therapist</p>
					<a class="arrow-next staff" href="#item-31"></a>
					</div>
			  </figure>
			  <figure class="item our-staff-1">
					<div>
					<a class="arrow-previous staff" href="#item-30"></a>
					<img src="/wp-content/themes/Rehab-And-Treatment/mobile-images/the-canyon-staff/Anna-Aslanian-Primary-Therapist.png"><p class="name">Anna Aslanian</p><p>Primary Therapist</p>
					<a class="arrow-next staff" href="#item-32"></a>
					</div>
			  </figure>
			  <figure class="item our-staff-1">
					<div>
					<a class="arrow-previous staff" href="#item-31"></a>
					<img src="/wp-content/themes/Rehab-And-Treatment/mobile-images/the-canyon-staff/Deborah-Rosenberg-Office-Manager.png"><p class="name">Deborah Rosenberg</p><p>Office Manager</p>
					<a class="arrow-next staff" href="#item-33"></a>
					</div>
			  </figure>
			  <figure class="item our-staff-1">
					<div>
					<a class="arrow-previous staff" href="#item-32"></a>
					<img src="/wp-content/themes/Rehab-And-Treatment/mobile-images/the-canyon-staff/Joel-Morris-Consulting-Psychiatrist.png"><p class="name">Joel Morris</p><p>Consulting Psychiatrist</p>
					<a class="arrow-next staff" href="#item-34"></a>
					</div>
			  </figure>

			  <figure class="item our-staff-1">
			  		<div>
			  		<a class="arrow-previous staff" href="#item-33"></a>
					<img src="/wp-content/themes/Rehab-And-Treatment/mobile-images/the-canyon-staff/lynn-marie-saso-ClinicalDirector.png"><p class="name">Lynn Marie Saso</p><p>Clinical Director</p>
					<a class="arrow-next staff" href="#item-35"></a>
					</div>
				</figure>
				<figure class="item our-staff-1">
					<div>
					<a class="arrow-previous staff" href="#item-34"></a>
					<img src="/wp-content/themes/Rehab-And-Treatment/mobile-images/the-canyon-staff/Melinda-Drake-Director.png"><p class="name">Melinda Drake</p><p>Director</p>
					<a class="arrow-next staff" href="#item-36"></a>
					</div>
				</figure>
				<figure class="item our-staff-1">
					<div>
					<a class="arrow-previous staff" href="#item-35"></a>
					<img src="/wp-content/themes/Rehab-And-Treatment/mobile-images/the-canyon-staff/Rodney-Aquino-Patient-Care-Coordinator.png"><p class="name">Rodney Aquino</p><p>Patient Care Coordinator</p>
					<a class="arrow-next staff" href="#item-30"></a>
					</div>
				</figure>

			  <div class="controls">
			    <a href="#item-30" class="control-button">•</a>
			    <a href="#item-31" class="control-button">•</a>
			    <a href="#item-32" class="control-button">•</a>
			    <a href="#item-33" class="control-button">•</a>
			    <a href="#item-34" class="control-button">•</a>
			    <a href="#item-35" class="control-button">•</a>
			    <a href="#item-36" class="control-button">•</a>
			  </div>
			</div>
		
	</section>

<style type="text/css">
		
	@media (min-width:1020px) {
		section.testimonials.chicago-staff.mobile-only{
			display: none !important
		}
		section.testimonials.desktop-tc.mobile-no{
			display: block !important;
			padding: 2em 2em 4em;
		}
	}

	@media (max-width:1019px) {
		section.testimonials.chicago-staff.mobile-only{
			display: block !important;
			padding-bottom: 4em;
		}
		a.arrow-next.staff {
	    	margin-top: -195px !important;
	    }
	    a.arrow-previous.staff {
	    	margin-top: 55px !important;
	    }
		section.testimonials.desktop-tc.mobile-no{
			display: none !important;
		}
	}

</style>